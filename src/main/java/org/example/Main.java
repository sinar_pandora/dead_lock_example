package org.example;

import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        final Object lockA = new Object();
        final Object lockB = new Object();

        List.of(Map.entry(lockA, lockB), Map.entry(lockB, lockA)).forEach(entry -> {
            new Thread(() -> {
                synchronized (entry.getKey()) {
                    System.out.println("Thread " + Thread.currentThread().getName() + " acquired lock " + entry.getKey());
                    synchronized (entry.getValue()) {
                        System.out.println("Thread " + Thread.currentThread().getName() + " acquired lock " + entry.getValue());
                    }
                }
            }).start();
        });

        System.out.println("Current PID is: " + ProcessHandle.current().pid());

        Runtime.getRuntime().addShutdownHook(new Thread(() ->
                System.out.println("Will not execute this line!"))
        );
    }
}
